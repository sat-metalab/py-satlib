import random

from .adjectives import adjectives
from .nouns import nouns


def generate_name() -> str:
    """
    Quick and dirty nickname generator

    :return: str
    """
    return random.choice(adjectives).capitalize() + " " + random.choice(nouns).capitalize()
