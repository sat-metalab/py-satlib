def bytes_to_hex(data: bytes, columns: int = 16, group: int = 4) -> str:
    """
    Return a table-formatted hex view of the bytes passed as argument
    with headers rows and columns in order to read the hex data.

    Hex dump, length: 18
       00 01 02 03 04  05 06 07 08  09 0a 0b 0c  0d 0e 0f 10
    00 01 45 49 53 00  10 01 04 00  cb 41 d6 57  a6 62 4d ce
    01 01 45 49 53 00  10 01 04 00  cb 41 d6 57  a6 62 4d ce
    02 01 45 49 53 00  10 01 04 00  cb 41 d6 57  a6 62 4d ce
    03 01 45 49 53 00  10 01 04 00  cb 41 d6 57  a6 62 4d ce

    (It looks better with terminal colors!)

    :param data: bytes - Data to show in a table
    :param columns: int - Number of columns to display the data in
    :param group: int - Group values by inserting gutters every x values
    :return: str - Table formatted view of the bytes data
    """

    if columns % 16 != 0:
        raise Exception("Number of columns should be a multiple of 16.")

    if group % 2 != 0:
        raise Exception("Group count should be even.")

    length = len(data)
    cols = min(length, columns)

    hex_str = "Hex dump, length: {}\n\033[1;30m   ".format(length)

    if length > 1024:
        hex_str += "Message too large!"
        return hex_str

    # Header cells
    for x in range(cols):
        hex_str += '{0:02x}'.format(x) + (" " if x == 0 or x % group != 0 else "  ")

    col = 0
    row = 0
    for b in data:
        # Header cell
        if col == 0:
            hex_str += "\n\033[1;30m" + '{0:02x}'.format(columns*row) + " \033[0;37m"

        # Hex data
        hex_str += '{0:02x}'.format(b) + (" " if col == 0 or col % group != 0 else "  ")

        col += 1
        if col >= columns:
            col = 0
            row += 1

    # Fill the rest of the row with dots
    if col > 0:
        hex_str += "\033[1;30m"
        for i in range(col, columns):
            hex_str += ". " + (" " if col == 0 or col % group != 0 else "  ")
            col += 1

    return hex_str
