from configparser import ConfigParser
from unittest import TestCase

from satlib.utils.config_utils import defaults


class TestConfigUtilsDefaults(TestCase):

    def test_none_config(self):
        config = defaults({'key': "value"})
        self.assertEqual(config['key'], "value")

    def test_config_without_key(self):
        parser = ConfigParser()
        parser.add_section('section')
        config = defaults({'key': "value"}, parser['section'])
        self.assertEqual(config['key'], "value")

    def test_config_with_key(self):
        parser = ConfigParser()
        parser.add_section('section')
        parser['section']['key'] = "another value"
        config = defaults({'key': "value"}, parser['section'])
        self.assertEqual(config['key'], "another value")

    def test_conversion(self):
        """ ConfigParser only does strings, so check that we properly convert values when settign as defaults"""
        parser = ConfigParser()
        parser.add_section('section')
        config = defaults({'str': "value", 'int': 1, 'float': 1.00}, parser['section'])
        self.assertEqual(config['str'], "value")
        self.assertEqual(config['int'], "1")
        self.assertEqual(config['float'], "1.0")